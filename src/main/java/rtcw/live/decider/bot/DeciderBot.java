package rtcw.live.decider.bot;

import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import rtcw.live.decider.bot.command.CommandMessageCreateListener;

public class DeciderBot {

    private static final String TOKEN = "NzQwNDIyODU0NTQ1NTA2MzY1.XyoyhA.YpfiWMDOzQH9MXTiZe-jqAOc3Oc";

    private static DeciderBot instance;

    private final DiscordApi api;

    private DeciderBot() {
        this.api = new DiscordApiBuilder().setToken(TOKEN).login().join();
    }

    public static DeciderBot getInstance() {

        if (instance == null) {
            instance = new DeciderBot();
        }

        return instance;
    }

    public void init() {
        api.addMessageCreateListener(new CommandMessageCreateListener());
    }

    public DiscordApi getApi() {
        return api;
    }

    public static void main(String[] args) {

        DeciderBot.getInstance().init();

        System.out.println("You can invite the bot by using the following url: " + DeciderBot.getInstance().getApi().createBotInvite());
//        https://discord.com/oauth2/authorize?client_id=740422854545506365&scope=bot&permissions=2048
    }
}
