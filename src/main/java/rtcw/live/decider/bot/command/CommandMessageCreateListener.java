package rtcw.live.decider.bot.command;

import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;
import rtcw.live.decider.bot.Commands;

public class CommandMessageCreateListener implements MessageCreateListener {

    @Override
    public void onMessageCreate(MessageCreateEvent event) {

        if (event.getMessageContent().startsWith(Commands.DECIDER.getCommand())) {

            Decider decider = new Decider();
            decider.doCommand(event);
        }
    }
}
