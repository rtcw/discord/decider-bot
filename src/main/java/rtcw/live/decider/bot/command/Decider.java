package rtcw.live.decider.bot.command;

import org.apache.commons.lang3.StringUtils;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.event.message.MessageCreateEvent;

import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Decider implements Command {

    @Override
    public void doCommand(MessageCreateEvent event) {

        String message = event.getMessageContent().replaceFirst("!decider", "");

        List<String> mapList = Arrays.asList(StringUtils.split(message, " "));

        if (mapList.size() == 2) {

            Random rand = new Random();
            String map = mapList.get(rand.nextInt(mapList.size()));

            new MessageBuilder()
                    .setEmbed(new EmbedBuilder()
                            .setTitle("Decider")
                            .setDescription(map)
                            .setColor(Color.decode("#ff671f")))
                    .send(event.getChannel());
        }
        else {
            new MessageBuilder()
                    .append("Please specify two maps. For example: ")
                    .appendCode("java", "!decider map1 map2")
                    .send(event.getChannel());
        }
    }
}