package rtcw.live.decider.bot.command;

import org.javacord.api.event.message.MessageCreateEvent;

public interface Command {

    void doCommand(MessageCreateEvent event);
}
