package rtcw.live.decider.bot;

public enum Commands {

    DECIDER("!decider");

    private final String command;

    Commands(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }
}
